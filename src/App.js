import BenefitQuote from './components/BenefitQuote';

const App = () => (
  <div style={{width:'70%', margin:'auto'}}>    
    <BenefitQuote />
  </div>
)

export default App;