import * as axios from 'axios';

const axiosInstance = axios.create({    
    baseURL: `${process.env.REACT_APP_BASE_URL}/benefits`,
})

export const createQuote = async (form) => {
    try {
        // api call
        const { data } = await axiosInstance.post('/quote', form);
        return data;
    } catch {
        console.log('Error!');
    }
}