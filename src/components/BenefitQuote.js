import React, { useState } from 'react';
import EmployeeForm from "./EmployeeForm";
import DependentsForm from "./DependentsForm";
import { createQuote } from '../services/quote';

const BenefitQuote = () => {
    const [employee, setEmployee] = useState({ firstName: '', middleName: '', lastName: '' });
    const [dependents, setDependents] = useState([]);
    const [quote, setQuote] = useState();

    return <>
        <form
            onSubmit={event => {
                event.preventDefault();
                createQuote({ employee, dependents })
                    .then((quote) => {
                        setQuote(quote);
                    });
            }}>
            <h3 style={{ textAlign: 'center' }}>Benefits Quote</h3>
            <EmployeeForm employee={employee} setEmployee={setEmployee} />
            <hr />
            <DependentsForm dependents={dependents} setDependents={setDependents} />
            <hr />
            <button type='submit' className='btn btn-success' style={{ margin: 'auto', display: 'block' }}>Submit Quote</button>
        </form >
        {quote === undefined
            ? <></>
            : <div>
                <hr />
                <h3 style={{ textAlign: 'center' }}>Benefits Quote Payments</h3>
                <ul>
                    <li>Yearly benefits total cost: ${quote.yearlyBenefitsCost}</li>
                    <li>Pay check deductions: ${quote.paycheckDeductions}</li>
                    <li>Net pay check amount: ${quote.netPaycheckAmount}</li>
                </ul>
            </div>
        }
    </>
}

export default BenefitQuote;