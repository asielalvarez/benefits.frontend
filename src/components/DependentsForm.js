import CustomerForm from './CustomerForm';

const DependentsForm = ({ dependents, setDependents }) => {
    const handleAdd = () => {
        setDependents([...dependents, { firstName: '', middleName: '', lastName: '' }]);
    };

    const handleRemove = index => {
        const list = [...dependents];
        list.splice(index, 1);
        setDependents(list);
    };

    const handleFormChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...dependents];
        list[index][name] = value;
        setDependents(list);
    };

    return <div>
        <h5>Dependents</h5>
        {dependents.map((customer, index) => (
            <div key={index}>
                <div style={{ margin: 'auto', display: 'flex' }}>
                    <CustomerForm handleFormChange={handleFormChange} customer={customer} index={index} />
                    <button type='button' className='btn btn-warning' style={{ float: 'left' }}
                     onClick={() => handleRemove(index)}>Remove</button>
                </div>
            </div>
        ))}
        <button type='button' className='btn btn-primary' style={{marginTop:'1rem'}} onClick={handleAdd}>Add Dependent</button>
    </div>
}

export default DependentsForm;