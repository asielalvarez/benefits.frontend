import CustomerForm from "./CustomerForm";

const EmployeeForm = ({employee, setEmployee}) => {
    const handleFormChange = (e) => {
        const { name, value } = e.target;
        const newEmployee = { ...employee };
        newEmployee[name] = value;
        setEmployee(newEmployee);
    }

    return <div style={{margin:'auto', width:'fit-content'}}>
        <h5>Employee</h5>
        <CustomerForm handleFormChange={handleFormChange} customer={employee} />
    </div>
}

export default EmployeeForm;