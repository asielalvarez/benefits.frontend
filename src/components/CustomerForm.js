const CustomerForm = ({ handleFormChange, customer, index }) => {
    return <>
        <div style={{ width: '25rem' }}>
            <input name="firstName" className="form-control" placeholder="First name"
                onChange={e => handleFormChange(e, index)} value={customer.firstName} />
        </div>
        <div style={{ width: '25rem' }}>
            <input name="middleName" className="form-control" placeholder="Middle name"
                onChange={e => handleFormChange(e, index)} value={customer.middleName} />
        </div>
        <div style={{ width: '25rem' }}>
            <input name="lastName" className="form-control" placeholder="Last name"
                onChange={e => handleFormChange(e, index)} value={customer.lastName} />
        </div>
    </>
}

export default CustomerForm;